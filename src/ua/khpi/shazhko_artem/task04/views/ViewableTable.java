package ua.khpi.shazhko_artem.task04.views;

import ua.khpi.shazhko_artem.task03.views.View;
import ua.khpi.shazhko_artem.task03.views.Viewable;

/**
 * fabricating object
 * 
 * @author Shzhko
 *
 */
public class ViewableTable implements Viewable {
	/**
	 * fabricate table view
	 * 
	 * @return fabricated object
	 */
	@Override
	public View getView() {
		return new ViewTable();
	}
}
