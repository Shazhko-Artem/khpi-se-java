package ua.khpi.shazhko_artem.task04;

import java.util.Scanner;

import ua.khpi.shazhko_artem.task03.menu.Menu;
import ua.khpi.shazhko_artem.task03.views.ViewableResult;
import ua.khpi.shazhko_artem.task04.views.ViewableTable;

public class Main {

	public static void main(String[] args) {
		int menuItem = 0;
		Scanner scanner = new Scanner(System.in);
		String format = "| %-29s |%n";

		do {
			System.out.println("+-------------------------------+");
			System.out.format(format,"Select view mode");
			System.out.println("+-------------------------------+");
			System.out.format(format,"1) Simple view.");
			System.out.format(format,"2) Table view.");
			System.out.format(format,"3) Exit.");
			System.out.println("+-------------------------------+");
			System.out.print(">>> ");
			menuItem = scanner.nextInt();
		} while (menuItem<0||menuItem>3);

		switch (menuItem) {
		case 1: {
			Menu menu = new Menu(new ViewableResult());
			menu.dialog();
			break;
		}
		case 2: {
			Menu menu = new Menu(new ViewableTable());
			menu.dialog();
			break;
		}
		default:
			break;
		}
		
		scanner.close();
	}

}
