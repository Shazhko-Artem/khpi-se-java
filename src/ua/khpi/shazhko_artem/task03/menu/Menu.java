package ua.khpi.shazhko_artem.task03.menu;

import java.io.IOException;
import java.util.Scanner;

import ua.khpi.shazhko_artem.task03.views.View;
import ua.khpi.shazhko_artem.task03.views.Viewable;

public class Menu {
	/**
	 * radix for input length
	 */
	public static int RADIX = 2;
	/**
	 * fabricated object
	 */
	private View view;

	/**
	 * constructor
	 * 
	 * @param viewable provide view
	 */
	public Menu(Viewable viewable) {
		this.view = viewable.getView();
	}

	/**
	 * dialog with customer for creating objects, serialization, deserialization *
	 */
	public void dialog() {
		int menuItem;
		Scanner scanner = new Scanner(System.in);

		do {
			System.out.println();
			System.out.println("+---------------------------------+");
			System.out.println("|            Menu                 |");
			System.out.println("+---------------------------------+");
			System.out.println("| 1) Initialize environment.      |");
			System.out.println("| 2) Show information.            |");
			System.out.println("| 3) Save.                        |");
			System.out.println("| 4) Load.                        |");
			System.out.println("| 5) Exit.                        |");
			System.out.println("+---------------------------------+");
			System.out.print(">>> ");
			menuItem = scanner.nextInt();

			switch (menuItem) {
			case 1:
				System.out.println("[Initialize environment] Process....");
				view.init();
				System.out.println("[Initialize environment] Done.");
				break;
			case 2:
				view.show();
				break;
			case 3:
				System.out.println("[Save] Process...");
				try {
					view.save();
					System.out.println("[Save] Done.");
				} catch (IOException e) {
					System.out.println("[ERROR] " + e.getMessage());
					e.printStackTrace();
				}
				break;
			case 4:
				System.out.println("[Load] Process...");
				try {
					view.restore();
					System.out.println("[Load] Done.");
				} catch (ClassNotFoundException e) {
					System.out.println("[ERROR] " + e.getMessage());
					e.printStackTrace();
				} catch (IOException e) {
					System.out.println("[ERROR] " + e.getMessage());
					e.printStackTrace();
				}
				break;
			default:
				break;
			}
		} while (menuItem != 5);
		System.out.println("[Exit] Done.");
		scanner.close();
	}
}