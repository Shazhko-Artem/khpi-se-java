package ua.khpi.shazhko_artem.task03.views;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import ua.khpi.shazhko_artem.task02.FileBasedRepository;
import ua.khpi.shazhko_artem.task02.Repository;
import ua.khpi.shazhko_artem.task02.biology.*;
import ua.khpi.shazhko_artem.task02.biology.Environment;

/**
 * fabricated object
 * 
 * @author Shazhko
 *
 */
public class ViewResult implements View {
	/**
	 * number of elements in collection
	 */
	public static final int DEFAULT_QUANTITY_STEPS = 5;
	/**
	 * file for storing information
	 */
	public static final String FILE_NAME = "task03.viewResult.ini";
	public static final int DEFAULT_EUKARYOTE_QUANTITY = 100;
	/**
	 * collection of "Store" object
	 */
	ArrayList<FissionSimulation.FissionResult> fissionResults;
	private int quantitySteps;
	private Repository repository;

	private Environment environment;

	/**
	 * default constructor
	 */
	public ViewResult() {
		fissionResults = new ArrayList<>();
		repository = new FileBasedRepository(FILE_NAME);
		quantitySteps = DEFAULT_QUANTITY_STEPS;
		environment = new Environment();
	}

	/**
	 * Constructor with param for Quantity of step
	 * @param quantitySteps Quantity of step
	 */
	public ViewResult(int quantitySteps) {
		this.quantitySteps = quantitySteps;
		fissionResults = new ArrayList<>();
		repository = new FileBasedRepository(FILE_NAME);
		environment = new Environment();
	}

	/**
	 * Constructor
	 * @param repository Repository for save and load fission results
	 * @param quantitySteps Quantity of step
	 */
	public ViewResult(Repository repository, int quantitySteps) {
		this.quantitySteps = quantitySteps;
		fissionResults = new ArrayList<>();
		this.repository = repository;
		environment = new Environment();
	}

	/**
	 * random initialization
	 */
	public void init() {
		environment.init(DEFAULT_EUKARYOTE_QUANTITY);
		for (int i = 0; i < this.quantitySteps; i++) {
			fissionResults.add(environment.binaryFissionEukaryotes());
		}
	}
	
	/**
	 * show information about current stage of object
	 */
	public void viewHeader() {
		System.out.format("+-----------------------------------------------+%n");
		System.out.format("| %-45s |%n", "Environment");
		System.out.format("+--------------------------------+--------------+%n");
		String leftAlignFormat = "| %-30s | %-12s |%n";

		System.out.format(leftAlignFormat, "Eukaryote quantity", environment.getEukaryoteQuantity());
		System.out.format(leftAlignFormat, "Is favorable conditions", environment.isFavorableConditions());

		System.out.format("+--------------------------------+--------------+%n");

	}

	/**
	 * show information about current stage of object
	 */
	public void viewBody() {
		System.out.format("+-----------------------------------------------+%n");
		System.out.format("| %-45s |%n", "Binary fissione ukaryotes");
		System.out.format("+--------------------------------+--------------+%n");

		String leftAlignFormat = "| | %-28s | %-10s | |%n";
		for (FissionSimulation.FissionResult fissionResult : fissionResults) {
			System.out.format("| +------------------------------+------------+ |%n");
			System.out.format(leftAlignFormat, "Step", "# " + fissionResult.getNumber());
			System.out.format(leftAlignFormat, "Eukaryotes count", fissionResult.getEukaryotesCount());
			System.out.format(leftAlignFormat, "Alive eukaryotes percentage",
					fissionResult.getAliveEukaryotesPercentage() + " %");
			System.out.format(leftAlignFormat, "Dead eukaryotes percentage",
					fissionResult.getDeadEukaryotesPercentage() + " %");
			System.out.format(leftAlignFormat, "Dead eukaryotes count", fissionResult.getDeadEukaryotesCount());
			System.out.format(leftAlignFormat, "Alive eukaryotes count", fissionResult.getAliveEukaryotesCount());
			System.out.format("| +------------------------------+------------+ |%n");
		}
	}

	/**
	 * show information about current stage of object
	 */
	public void viewFooter() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();

		System.out.format("+-----------------------------------------------+%n");
		System.out.format("| %-45s |%n", "Date : " + dtf.format(now));
		System.out.format("+-----------------------------------------------+%n");
	}

	/**
	 * show information
	 */
	public void show() {
		viewHeader();
		viewBody();
		viewFooter();
	}
 
	/**
	 * serializing
	 * 
	 * @throws IOException
	 */
	@Override
	public void save() throws IOException {
		repository.save(fissionResults);
	}

	/**
	 * deserializing
	 * 
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public void restore() throws IOException, ClassNotFoundException {
		fissionResults = (ArrayList<FissionSimulation.FissionResult>) repository.load();
	}
}