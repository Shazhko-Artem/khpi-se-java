package ua.khpi.shazhko_artem.task03.views;

/**
 * fabricating object interface
 * 
 * @author Shazhko
 *
 */
public interface Viewable {
	/**
	 * fabricate view
	 * 
	 * @return fabricated object
	 */
	View getView();
}
