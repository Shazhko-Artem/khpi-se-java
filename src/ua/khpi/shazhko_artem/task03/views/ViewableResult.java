package ua.khpi.shazhko_artem.task03.views;

/**
 * fabricating object
 * 
 * @author Shzhko
 *
 */ 
public class ViewableResult implements Viewable {
	/**
	 * fabricate view
	 * 
	 * @return fabricated object
	 */
	public View getView() {
		return new ViewResult();
	}
}
