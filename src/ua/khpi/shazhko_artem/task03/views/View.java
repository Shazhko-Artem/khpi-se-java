package ua.khpi.shazhko_artem.task03.views;

import java.io.IOException;

/**
 * Fabricated objects interface
 * 
 * @author Shazhko
 *
 */
public interface View {
	/**
	 * show information about current stage of object
	 */
	public void viewHeader();

	/**
	 * show information about current stage of object
	 */
	public void viewBody();

	/**
	 * show information about current stage of object
	 */
	public void viewFooter();

	/**
	 * show information about current stage of object
	 */
	public void show();

	/**
	 * random initializing
	 */
	public void init();

	/**
	 * serializing
	 * 
	 * @throws IOException
	 */
	public void save() throws IOException;

	/**
	 * deserializing
	 * 
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public void restore() throws IOException, ClassNotFoundException;

}
