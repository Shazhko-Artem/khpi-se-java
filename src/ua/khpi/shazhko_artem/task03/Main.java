package ua.khpi.shazhko_artem.task03;

import ua.khpi.shazhko_artem.task03.menu.Menu;
import ua.khpi.shazhko_artem.task03.views.ViewableResult;

public class Main {
	public static void main(String[] args) {
		Menu menu = new Menu(new ViewableResult());
		menu.dialog();
	}
}
