package ua.khpi.shazhko_artem.task08.views;

import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.Map;


/**
 * Diagram window
 * 
 * @author Shazhko
 *
 */
public class DiagramWindow extends Frame {

	private static final long serialVersionUID = 1L;
	/**
	 * margin between window borders and information
	 */
	private Map<String, Double> values;

	/**
	 * constructor
	 * 
	 * @param view fabricated object
	 */
	public DiagramWindow() {
		this.values = new HashMap<>();
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				setVisible(false);
			}
		});
	}

	public void addValue(String key, double value) throws Exception {
		if (value < 0 || value > 100)
			throw new Exception(String.format("The value for the key \"T\" is invalid.", key));

		values.put(key, value);
	}

	public void clearValues() {
		values.clear();
	}

	/**
	 * main window activity
	 */
	@Override
	public void paint(Graphics g) {
		int diagramSize = 200;
		double step = 360.0 / 100;
		int acrOffset = 0, offsetY = 140;

		for (Map.Entry<String, Double> entry : values.entrySet()) {
			String key = entry.getKey();
			double valueInPercent = entry.getValue();

			int value = (int) (valueInPercent * step);
			int red = (int) (Math.random() * 100), green = (int) (100 + Math.random() * 50),
					blue = (int) (Math.random() * 100);
			
			g.setColor(new Color(red, green, blue));
			g.fillArc(50, 100, diagramSize, diagramSize, acrOffset, value);
			acrOffset += value;

			g.fillRect(diagramSize + 80, offsetY, 10, 10);

			g.setColor(Color.BLACK);

			g.drawString(key+"  ("+valueInPercent+" %)", diagramSize + 100, offsetY+9);
			offsetY += 30;
		}
	}
}
