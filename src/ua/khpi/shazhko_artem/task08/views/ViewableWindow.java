package ua.khpi.shazhko_artem.task08.views;

import ua.khpi.shazhko_artem.task06.views.View;
import ua.khpi.shazhko_artem.task06.views.Viewable;

/**
 * fabricate object
 * 
 * @author Shazhko
 *
 */
public class ViewableWindow implements Viewable {
	/**
	 * @return fabricated object
	 */
	@Override
	public View getView() {
		return new ViewWindow();
	}

}
