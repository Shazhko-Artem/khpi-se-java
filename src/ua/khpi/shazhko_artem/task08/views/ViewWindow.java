package ua.khpi.shazhko_artem.task08.views;

//import java.awt.Dimension;
import java.awt.*;

import ua.khpi.shazhko_artem.task02.biology.FissionSimulation.FissionResult;
import ua.khpi.shazhko_artem.task06.views.*;
import java.io.IOException;
import java.util.ArrayList;

/**
 * fabricated object for window representation
 * 
 * @author Shazhko
 *
 */
public class ViewWindow extends ViewResult {
	/**
	 * number of elements in collection
	 */
	public static final int DEFAULT_QUANTITY_STEPS = 5;
	/**
	 * window object
	 */
	private DiagramWindow window = null;

	/**
	 * default constructor
	 */
	public ViewWindow() {
		super(DEFAULT_QUANTITY_STEPS);
		window = new DiagramWindow();
		window.setSize(new Dimension(580, 400));
		window.setTitle("Squares");
	}

	/**
	 * show information
	 */
	@Override
	public void show() {
		super.show();
		window.setVisible(true);
		window.repaint();

	}

	public void init(){
		super.init();

		addValueToWindow();
		window.repaint();
	}

	private void addValueToWindow() {
		window.clearValues();
		
		ArrayList<FissionResult> fissionResults=this.getFissionsResults();
		FissionResult lastItem=fissionResults.get(fissionResults.size()-1);
		try {
			window.addValue("Alive eukaryotes percentage", lastItem.getAliveEukaryotesPercentage());
			window.addValue("Dead eukaryotes percentage", lastItem.getDeadEukaryotesPercentage());
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	}
	
	public void restore() throws IOException, ClassNotFoundException {
		super.restore();
		addValueToWindow();
		 window.repaint();
	}

	@Override
	public void viewBody() {
		window.setVisible(true);
	}

}
