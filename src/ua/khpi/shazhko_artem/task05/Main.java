package ua.khpi.shazhko_artem.task05;

import java.util.Scanner;
import ua.khpi.shazhko_artem.task03.views.View;
import ua.khpi.shazhko_artem.task03.views.ViewableResult;
import ua.khpi.shazhko_artem.task04.views.ViewableTable;

/**
 * Main class
 * 
 * @author Shazhko
 *
 */
public class Main {
	public static void main(String[] args) {
		int menuItem = 0;
		Scanner scanner = new Scanner(System.in);
		String format = "| %-29s |%n";

		do {
			System.out.println("+-------------------------------+");
			System.out.format(format, "Select view mode");
			System.out.println("+-------------------------------+");
			System.out.format(format, "1) Simple view.");
			System.out.format(format, "2) Table view.");
			System.out.format(format, "3) Exit.");
			System.out.println("+-------------------------------+");
			System.out.print(">>> ");
			menuItem = scanner.nextInt();
		} while (menuItem < 0 || menuItem > 3);

		View view = null;

		switch (menuItem) {
		case 1: {
			view = new ViewableResult().getView();
			break;
		}
		case 2: {
			view = new ViewableTable().getView();
			break;
		}
		default:
			break;
		}

		Application application = Application.getInstance();

		application.run(view);

		scanner.close();
	}
}
