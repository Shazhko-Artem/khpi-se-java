package ua.khpi.shazhko_artem.task05.menu.commands;

import java.io.IOException;

import ua.khpi.shazhko_artem.task03.views.View;

/**
 * Save command
 * 
 * @author Shazhko
 *
 */
public class SaveCommand implements ConsoleCommand {
	private int menuKey;
	private View view;

	/**
	 * Initializes the SaveCommand
	 * 
	 * @param key  Menu key
	 * @param view view object
	 */
	public SaveCommand(int key, View view) {
		this.menuKey = key;
		this.view = view;
	}

	/**
	 * command activity
	 */
	@Override
	public void execute() {
		System.out.println("[Save] Process...");
		try {
			view.save();
			System.out.println("[Save] Done.");
		} catch (IOException e) {
			System.out.println("[ERROR] " + e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @return command key
	 */
	@Override
	public int getKey() {
		return menuKey;
	}

	@Override
	public String toString() {
		return "Save.";
	}
}
