package ua.khpi.shazhko_artem.task05.menu.commands;

import java.io.IOException;

import ua.khpi.shazhko_artem.task03.views.View;

/**
 * Load command
 * 
 * @author Shazhko
 *
 */
public class LoadCommand implements ConsoleCommand {
	private int menuKey;
	private View view;

	/**
	 * Initializes the LoadCommand
	 * 
	 * @param key  Menu key
	 * @param view view object
	 */
	public LoadCommand(int key, View view) {
		this.menuKey = key;
		this.view = view;
	}

	/**
	 * command activity
	 */
	@Override
	public void execute() {
		System.out.println("[Load] Process...");
		try {
			view.restore();
			System.out.println("[Load] Done.");
		} catch (IOException e) {
			System.out.println("[ERROR] " + e.getMessage());
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			System.out.println("[ERROR] " + e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @return command key
	 */
	@Override
	public int getKey() {
		return menuKey;
	}

	@Override
	public String toString() {
		return "Load.";
	}
}
