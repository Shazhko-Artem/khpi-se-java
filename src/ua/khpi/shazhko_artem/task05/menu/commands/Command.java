package ua.khpi.shazhko_artem.task05.menu.commands;

/**
 * interface of command
 * 
 * @author Shazhko
 *
 */
public interface Command {
	/**
	 * command activity
	 */
	public void execute();
}
