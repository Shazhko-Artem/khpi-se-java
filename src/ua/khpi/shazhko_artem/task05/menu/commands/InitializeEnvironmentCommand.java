package ua.khpi.shazhko_artem.task05.menu.commands;

import ua.khpi.shazhko_artem.task03.views.View;

/**
 * Initialize environment command
 * 
 * @author Shazhko
 *
 */
public class InitializeEnvironmentCommand implements ConsoleCommand {

	private int menuKey;
	private View view;
	
	/**
	 * Initializes the InitializeEnvironmentCommand
	 * 
	 * @param key Menu key
	 * @param view view object
	 */
	public InitializeEnvironmentCommand(int key,View view) {
		this.menuKey=key;
		this.view=view;
	}
	
	/**
	 * command activity
	 */
	@Override
	public void execute() {
		System.out.println("[Initialize environment] Process....");
		this.view.init();
		System.out.println("[Initialize environment] Done.");
	}

	/**
	 * 
	 * @return command key
	 */
	@Override
	public int getKey() {
		return menuKey;
	}
	
	@Override
	public String toString() {
		return "Initialize environment.";
	}

}
