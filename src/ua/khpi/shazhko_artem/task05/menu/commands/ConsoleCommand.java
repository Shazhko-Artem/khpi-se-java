package ua.khpi.shazhko_artem.task05.menu.commands;


/**
 * console Command
 * 
 * @author Shazhko
 *
 */
public interface ConsoleCommand extends Command {
	/**
	 * 
	 * @return command key
	 */
	public int getKey();
}
