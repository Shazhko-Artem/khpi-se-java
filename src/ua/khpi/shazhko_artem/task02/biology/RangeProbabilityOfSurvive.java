package ua.khpi.shazhko_artem.task02.biology;

import java.io.Serializable;

/**
 * RangeProbabilityOfSurvive
 * 
 * @author Shazhko
 *
 */
public class RangeProbabilityOfSurvive implements Serializable {

	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = 2920772563475475233L;
	private double minProbabilityOfSurvive;
	private double maxProbabilityOfSurvive;

	/**
	 * Initializes the RangeProbabilityOfSurvive
	 * 
	 * @param minProbabilityOfSurvive Min probability of survive
	 * @param maxProbabilityOfSurvive Max probability of survive
	 */
	public RangeProbabilityOfSurvive(double minProbabilityOfSurvive, double maxProbabilityOfSurvive) {
		this.minProbabilityOfSurvive = minProbabilityOfSurvive;
		this.maxProbabilityOfSurvive = maxProbabilityOfSurvive;
	}

	/**
	 * Get min probability of survive
	 * 
	 * @return min probability of survive
	 */
	public double getMinProbabilityOfSurvive() {
		return this.minProbabilityOfSurvive;
	}

	/**
	 * Set min probability of survive
	 * 
	 * @param minProbabilityOfSurvive min probability of survive
	 */
	public void setMinProbabilityOfSurvive(double minProbabilityOfSurvive) {
		this.minProbabilityOfSurvive = minProbabilityOfSurvive;
	}

	/**
	 * Get max probability of survive
	 * 
	 * @return maxProbabilityOfSurvive max probability of survive
	 */
	public double getMaxProbabilityOfSurvive() {
		return this.maxProbabilityOfSurvive;
	}

	/**
	 * Set max probability of survive
	 * 
	 * @param maxProbabilityOfSurvive set max probability of survive
	 */
	public void setMaxProbabilityOfSurvive(double maxProbabilityOfSurvive) {
		this.maxProbabilityOfSurvive = maxProbabilityOfSurvive;
	}

	/**
	 * Randomize probability of survive
	 * 
	 * @return generated value
	 */
	public double randomizeProbabilityOfSurvive() {
		return ua.khpi.shazhko_artem.task02.math.Util.getRandomDoubleBetweenRange(this.minProbabilityOfSurvive,
				this.maxProbabilityOfSurvive);
	}
}
