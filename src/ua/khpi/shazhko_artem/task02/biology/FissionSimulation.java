package ua.khpi.shazhko_artem.task02.biology;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * FissionSimulation
 * 
 * @author Shazhko
 *
 */
public class FissionSimulation implements Serializable {

	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = -6294268819228506409L;

	/**
	 * FissionResult
	 * 
	 * @author Shazhko
	 *
	 */
	public class FissionResult implements Serializable {
		/**
		 * Serial version UID
		 */
		private static final long serialVersionUID = -311117750549703559L;
		private int number;
		private Collection<Eukaryote> aliveEukaryote;
		private Collection<Eukaryote> deadEukaryote;

		/**
		 * Initializes the FissionResult
		 * 
		 * @param number     The number of step
		 * @param eukaryotes Collection of eukaryotes
		 */
		public FissionResult(int number, Collection<Eukaryote> eukaryotes) {
			aliveEukaryote = eukaryotes.stream().filter(new Predicate<Eukaryote>() {
				@Override
				public boolean test(Eukaryote eukaryote) {
					return eukaryote.isAlive();
				}
			}).collect(Collectors.toList());
			this.number = number;

			deadEukaryote = eukaryotes.stream().filter(new Predicate<Eukaryote>() {
				@Override
				public boolean test(Eukaryote eukaryote) {
					return !eukaryote.isAlive();
				}
			}).collect(Collectors.toList());

			this.number = number;
		}

		/**
		 * Get number of step
		 * 
		 * @return Step number
		 */
		public int getNumber() {
			return number;
		}

		/**
		 * Get alive eukaryotes count
		 * 
		 * @return alive eukaryotes count
		 */
		public int getAliveEukaryotesCount() {
			return aliveEukaryote.size();
		}

		/**
		 * Get dead eukaryotes count
		 * 
		 * @return dead eukaryotes count
		 */
		public int getDeadEukaryotesCount() {
			return deadEukaryote.size();
		}

		public int getEukaryotesCount() {
			return getAliveEukaryotesCount() + getDeadEukaryotesCount();
		}

		/**
		 * Get alive eukaryotes percentage
		 * 
		 * @return alive eukaryotes percentage
		 */
		public double getAliveEukaryotesPercentage() {
			if (getAliveEukaryotesCount() < 1)
				return 0;

			double percentage = ((double) getAliveEukaryotesCount() / getEukaryotesCount()) * 100;
			return Math.round(percentage * 100.0) / 100.0;
		}

		/**
		 * Get dead eukaryotes percentage
		 * 
		 * @return ead eukaryotes percentage
		 */
		public double getDeadEukaryotesPercentage() {
			if (getDeadEukaryotesCount() < 1)
				return 0;

			double percentage = ((double) getDeadEukaryotesCount() / getEukaryotesCount()) * 100;
			return Math.round(percentage * 100.0) / 100.0;
		}
	}

	private int currentStep = 0;
	private RangeProbabilityOfSurvive favorableConditions;
	private RangeProbabilityOfSurvive notFavorableConditions;

	/**
	 * Initializes the FissionSimulation
	 * 
	 * @param favorableConditions    Set range probability of survive for favorable
	 *                               conditions
	 * @param notFavorableConditions Set range probability of survive for not
	 *                               favorable conditions
	 */
	public FissionSimulation(RangeProbabilityOfSurvive favorableConditions,
			RangeProbabilityOfSurvive notFavorableConditions) {
		this.favorableConditions = favorableConditions;
		this.notFavorableConditions = notFavorableConditions;
	}

	/**
	 * Simulate eukaryotes death
	 * 
	 * @param environment Environment
	 * @param eukaryotes  Eukaryotes
	 */
	public void simulateEukaryotesDeath(Environment environment, Collection<Eukaryote> eukaryotes) {
		double probabilityOfDeath;
		if (environment.isFavorableConditions())
			probabilityOfDeath = 1 - favorableConditions.randomizeProbabilityOfSurvive();
		else
			probabilityOfDeath = 1 - notFavorableConditions.randomizeProbabilityOfSurvive();

		int deadEukaryotesCount = (int) (eukaryotes.size() * probabilityOfDeath);

		for (Eukaryote eukaryote : eukaryotes) {
			if (deadEukaryotesCount > 0)
				eukaryote.die();
			deadEukaryotesCount--;
		}
	}

	/**
	 * Simulate eukaryotes fission
	 * 
	 * @param environment Environment
	 * @param eukaryotes  Eukaryotes
	 * @return Fission result
	 */
	public FissionResult simulateEukaryotesFission(Environment environment, Collection<Eukaryote> eukaryotes) {
		LinkedList<Eukaryote> newGeneration = new LinkedList<>();
		for (Eukaryote eukaryote : eukaryotes) {
			for (Eukaryote childEukaryote : eukaryote.binaryFission()) {
				newGeneration.push(childEukaryote);
			}
		}

		simulateEukaryotesDeath(environment, newGeneration);
		eukaryotes.clear();
		eukaryotes.addAll(newGeneration);

		currentStep++;
		return new FissionSimulation.FissionResult(currentStep, newGeneration);
	}
}
