package ua.khpi.shazhko_artem.task02.biology;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;

/**
 * Eukaryote
 * 
 * @author Shazhko
 *
 */
public class Eukaryote implements Serializable {

	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = 1L;

	private boolean is_alive;
	private int number;

	/**
	 * Initializes the Eukaryote
	 */
	public Eukaryote() {
		is_alive = true;
	}

	/**
	 * Initializes the Eukaryote
	 * 
	 * @param number The number of eukaryote
	 */
	public Eukaryote(int number) {
		is_alive = true;
		this.number = number;
	}

	/**
	 * Is alive
	 * 
	 * @return isAlive
	 */
	public boolean isAlive() {
		return is_alive;
	}

	/**
	 * Get number
	 * 
	 * @return number
	 */
	public int getNumber() {
		return number;
	}

	/**
	 * Kill the eukaryote
	 */
	public void die() {
		is_alive = false;
	}

	/**
	 * Binary fission
	 * 
	 * @return Collection of new eukaryote after binary fission
	 */
	public Collection<Eukaryote> binaryFission() {
		LinkedList<Eukaryote> eukaryotes = new LinkedList<>();
		eukaryotes.add(new Eukaryote((int) (Math.random() * 1000)));
		eukaryotes.add(new Eukaryote((int) (Math.random() * 1000)));

		return eukaryotes;
	}
}
