package ua.khpi.shazhko_artem.task02.biology;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import ua.khpi.shazhko_artem.task02.FileBasedRepository;
import ua.khpi.shazhko_artem.task02.Repository;
import ua.khpi.shazhko_artem.task02.biology.FissionSimulation.FissionResult;

/**
 * Environment
 * 
 * @author Shazhko
 *
 */
public class Environment implements Serializable {
	/**
	 * serial version UID
	 */
	private static final long serialVersionUID = -8934555379681849253L;

	private boolean isFavorableConditions;
	private FissionSimulation fissionSimulation;
	private transient Repository repository = new FileBasedRepository("environment.ini");
	private Collection<Eukaryote> eukaryotes = new LinkedList<Eukaryote>();

	/**
	 * @return Eukaryote quantity
	 */
	public int getEukaryoteQuantity() {
		return eukaryotes.size();
	}

	/**
	 * @return Is favorable conditions
	 */
	public boolean isFavorableConditions() {
		return this.isFavorableConditions;
	}

	/**
	 * Initializes the Environment
	 */
	public Environment() {
		this.fissionSimulation = new FissionSimulation(new RangeProbabilityOfSurvive(0.5, 0.9),
				new RangeProbabilityOfSurvive(0.1, 0.5));
	}

	/**
	 * Initializes the Environment
	 * 
	 * @param repository Repository for save and load data
	 */
	public Environment(Repository repository) {
		this.repository = repository;
		this.fissionSimulation = new FissionSimulation(new RangeProbabilityOfSurvive(0.5, 0.9),
				new RangeProbabilityOfSurvive(0.1, 0.5));
	}

	/**
	 * Initializes the Environment
	 * 
	 * @param isFavorableConditions Set environment conditions
	 */
	public Environment(boolean isFavorableConditions) {
		this.isFavorableConditions = isFavorableConditions;
		this.fissionSimulation = new FissionSimulation(new RangeProbabilityOfSurvive(0.5, 0.9),
				new RangeProbabilityOfSurvive(0.1, 0.5));
	}

	/**
	 * Initializes the Environment
	 * 
	 * @param repository            Repository for save and load data
	 * @param isFavorableConditions Set environment conditions
	 */
	public Environment(Repository repository, boolean isFavorableConditions) {
		this.repository = repository;
		this.isFavorableConditions = isFavorableConditions;
		this.fissionSimulation = new FissionSimulation(new RangeProbabilityOfSurvive(0.5, 0.9),
				new RangeProbabilityOfSurvive(0.1, 0.5));
	}

	/**
	 * Binary fission eukaryotes
	 * 
	 * @return Fission result
	 */
	public FissionResult binaryFissionEukaryotes() {
		FissionResult fissionResult = fissionSimulation.simulateEukaryotesFission(this, eukaryotes);
		clearDeadEukaryots();
		return fissionResult;
	}

	/**
	 * Initializes environment with eukaryote
	 * 
	 * @param eukaryoteCount Quantity of eukaryotes
	 */
	public void init(int eukaryoteCount) {
		eukaryotes.clear();
		for (int i = 0; i < eukaryoteCount; i++)
			eukaryotes.add(new Eukaryote(i));
	}

	/**
	 * Save eukaryotes
	 */
	public void save() throws IOException {
		repository.save(this);
	}

	/**
	 * Load eukaryotes
	 */
	public void load() throws ClassNotFoundException, IOException {
		Object object = repository.load();
		Environment env = (Environment) object;
		this.eukaryotes = env.eukaryotes;
		this.fissionSimulation = env.fissionSimulation;
		this.isFavorableConditions = env.isFavorableConditions;
	}

	/**
	 * Clear dead eukaryots
	 */
	public void clearDeadEukaryots() {
		eukaryotes = eukaryotes.stream().filter(new Predicate<Eukaryote>() {
			@Override
			public boolean test(Eukaryote eukaryote) {
				return eukaryote.isAlive();
			}
		}).collect(Collectors.toList());
	}
}
