package ua.khpi.shazhko_artem.task02;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * File based repository
 * 
 * @author Shazhko
 *
 */
public class FileBasedRepository implements Repository {
	private String fileName;

	/**
	 * Initializes the FileBasedRepository
	 * 
	 * @param fileName File name
	 */
	public FileBasedRepository(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * Save object
	 * 
	 * @param object Object for save
	 */
	@Override
	public void save(Object object) throws IOException {
		try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(this.fileName))) {
			oos.writeObject(object);
		} catch (IOException e) {
			throw e;
		}
	}

	/**
	 * Load object
	 * 
	 * @return Loaded object
	 */
	@Override
	public Object load() throws IOException, ClassNotFoundException {
		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(this.fileName))) {
			return ois.readObject();
		} catch (Exception e) {
			System.out.println("[Error] " + e.getMessage());
			throw e;
		}
	}

}
