package ua.khpi.shazhko_artem.task02;

import ua.khpi.shazhko_artem.task02.menu.Menu;

/**
 * Print command-line parameters.
 */
public class Main {
	/**
	 * Program entry point.
	 * 
	 * @param args command-line parameters list
	 */
	public static void main(String[] args) {
		Menu.dialog();
	}
}
