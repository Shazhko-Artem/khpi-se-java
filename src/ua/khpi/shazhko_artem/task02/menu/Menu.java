package ua.khpi.shazhko_artem.task02.menu;

import java.io.IOException;
import java.util.Scanner;

import ua.khpi.shazhko_artem.task02.FileBasedRepository;
import ua.khpi.shazhko_artem.task02.biology.Environment;
import ua.khpi.shazhko_artem.task02.biology.FissionSimulation;

/**
 * Menu
 * 
 * @author Shazhko
 *
 */
public class Menu {

	/**
	 * Show dialog
	 */
	public static void dialog() {
		int menuItem;
		Environment environment = new Environment(new FileBasedRepository("environment.in"));
		Scanner scanner = new Scanner(System.in);

		do {
			System.out.println();
			System.out.println("+---------------------------------+");
			System.out.println("|            Menu                 |");
			System.out.println("+---------------------------------+");
			System.out.println("| 1) Initialize environment.      |");
			System.out.println("| 2) Show eukaryotic count.      |");
			System.out.println("| 3) Binary fission eukaryotes.   |");
			System.out.println("| 4) Save.                        |");
			System.out.println("| 5) Load.                        |");
			System.out.println("| 6) Exit.                        |");
			System.out.println("+---------------------------------+");
			System.out.print(">>> ");

			menuItem = scanner.nextInt();

			switch (menuItem) {
			case 1:
				System.out.print("[Initialize environment] Enter eukaryotic count : ");
				int eukaryoticCount = scanner.nextInt();
				environment.init(eukaryoticCount);
				System.out.println("[Initialize environment] Done..");
				break;
			case 2:
				System.out.println("[Show eukaryotic count] In the environment are "
						+ environment.getEukaryoteQuantity() + " eukaryotes");
				break;
			case 3:
				System.out.print("[Binary fission eukaryotes] Enter the number of binary fission cycle : ");
				int binaryFissionCount = scanner.nextInt();

				System.out.println("[Binary fission eukaryotes] Process...");

				String leftAlignFormat = "| %-30s | %-8s |%n";

				System.out.format("+--------------------------------+----------+%n");

				for (int i = 0; i < binaryFissionCount; i++) {
					FissionSimulation.FissionResult fissionResult = environment.binaryFissionEukaryotes();
					System.out.format(leftAlignFormat, "Step", "# " + fissionResult.getNumber());
					System.out.format(leftAlignFormat, "Eukaryotes count", fissionResult.getEukaryotesCount());
					System.out.format(leftAlignFormat, "Alive eukaryotes percentage",
							fissionResult.getAliveEukaryotesPercentage() + " %");
					System.out.format(leftAlignFormat, "Dead eukaryotes percentage",
							fissionResult.getDeadEukaryotesPercentage() + " %");
					System.out.format(leftAlignFormat, "Dead eukaryotes count", fissionResult.getDeadEukaryotesCount());
					System.out.format(leftAlignFormat, "Alive eukaryotes count",
							fissionResult.getAliveEukaryotesCount());
					System.out.format("+--------------------------------+----------+%n");
				}
				System.out.println("[Binary fission eukaryotes] Done.");
				break;
			case 4:
				System.out.println("[Save] Process...");
				try {
					environment.save();
					System.out.println("[Save] Done.");
				} catch (IOException e) {
					System.out.println("[ERROR] " + e.getMessage());
					e.printStackTrace();
				}
				break;
			case 5:
				System.out.println("[Load] Process...");
				try {
					environment.load();
					System.out.println("[Save] Done.");
				} catch (ClassNotFoundException e) {
					System.out.println("[ERROR] " + e.getMessage());
					e.printStackTrace();
				} catch (IOException e) {
					System.out.println("[ERROR] " + e.getMessage());
					e.printStackTrace();
				}
				break;
			default:
				break;
			}
		} while (menuItem != 6);
		System.out.println("[Exit] Done.");
		scanner.close();
	}
}
