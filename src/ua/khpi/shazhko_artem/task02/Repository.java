package ua.khpi.shazhko_artem.task02;

import java.io.IOException;

/**
 * Repository
 * 
 * @author Shazhko
 *
 */
public interface Repository {

	/**
	 * Save object
	 * 
	 * @param object Object for save
	 */
	void save(Object object) throws IOException;

	/**
	 * Load object
	 * 
	 * @return Loaded object
	 */
	Object load() throws IOException, ClassNotFoundException;
}
