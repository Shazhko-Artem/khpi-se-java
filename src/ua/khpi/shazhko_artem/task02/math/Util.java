package ua.khpi.shazhko_artem.task02.math;

/**
 * Math class
 * 
 * @author Shazhko
 *
 */
public abstract class Util {

	/**
	 * Get random double between range
	 * 
	 * @param min Min
	 * @param max Max
	 * @return generated value
	 */
	public static double getRandomDoubleBetweenRange(double min, double max) {
		return Math.random() * (max - min) + min;
	}
}
