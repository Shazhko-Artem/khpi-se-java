package ua.khpi.shazhko_artem.task06.views;

import java.io.IOException;
import java.util.ArrayList;
import ua.khpi.shazhko_artem.task02.biology.FissionSimulation;

/**
 * fabricated objects interface
 * 
 * @author Daria
 *
 */
public interface View {
	/**
	 * show information about current stage of object
	 */
	public void viewHeader();

	/**
	 * show information about current stage of object
	 */
	public void viewBody();

	/**
	 * show information about current stage of object
	 */
	public void viewFooter();

	/**
	 * show information about current stage of object
	 */
	public void show();

	/**
	 * random initializing
	 */
	public void init();

	public ArrayList<FissionSimulation.FissionResult> getFissionsResults();

	/**
	 * serializing
	 * 
	 * @throws IOException
	 */
	public void save() throws IOException;

	/**
	 * deserializing
	 * 
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public void restore() throws IOException, ClassNotFoundException;
}
