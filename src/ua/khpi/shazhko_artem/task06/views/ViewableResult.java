package ua.khpi.shazhko_artem.task06.views;

/**
 * fabricating object
 * @author Daria
 *
 */
public class ViewableResult implements Viewable{
	/**
	 * fabricate view
	 * @return fabricated object
	 */
	public View getView(){
		return new ViewResult();
	}
}
