package ua.khpi.shazhko_artem.task06.views;

import ua.khpi.shazhko_artem.task06.views.View;

public class ViewableTable {
	/**
	 * fabricate table view
	 * @return fabricated object
	 */
	public View getView(){
		return new ViewTable();
	}
}
