package ua.khpi.shazhko_artem.task06;

import ua.khpi.shazhko_artem.task06.views.View;
import ua.khpi.shazhko_artem.task06.menu.Menu;
import ua.khpi.shazhko_artem.task06.menu.commands.DemonstrateAsyncOperationsCommand;
import ua.khpi.shazhko_artem.task06.menu.commands.InitializeEnvironmentCommand;
import ua.khpi.shazhko_artem.task06.menu.commands.LoadCommand;
import ua.khpi.shazhko_artem.task06.menu.commands.SaveCommand;
import ua.khpi.shazhko_artem.task06.menu.commands.ShowInformationCommand;

/**
 * pattern Singleton
 * 
 * @author Shazhko
 *
 */
public class Application {
	private static Application instance = new Application();

	/**
	 * private default constructor
	 */
	private Application() {
	}

	/**
	 * method for getting single object
	 * 
	 * @return Application
	 */
	public static Application getInstance() {
		return instance;
	}

	//private View view = new ViewableTable().getView();
	private Menu menu = new Menu();

	/**
	 * create and run Menu
	 */
	public void run(View view) {
		menu.clear();
		menu.add(new InitializeEnvironmentCommand(1,view));
		menu.add(new ShowInformationCommand(2,view));
		menu.add(new DemonstrateAsyncOperationsCommand(3,view));
		menu.add(new SaveCommand(4,view));
		menu.add(new LoadCommand(5,view));
		menu.execute();
	}
}
