package ua.khpi.shazhko_artem.task06.menu.commands;

import java.io.IOException;

import ua.khpi.shazhko_artem.task06.views.View;
import ua.khpi.shazhko_artem.task05.menu.commands.ConsoleCommand;

/**
 * Load Command
 * 
 * @author Shazhko
 *
 */
public class LoadCommand implements ConsoleCommand {
	private int menuKey;
	private View view;
	
	public LoadCommand(int key,View view) {
		this.menuKey=key;
		this.view=view;
	}
	@Override
	public void execute() {
		System.out.println("[Load] Process...");
		try {
			view.restore();
			System.out.println("[Load] Done.");
		} catch (IOException e) {
			System.out.println("[ERROR] " + e.getMessage());
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			System.out.println("[ERROR] " + e.getMessage());
			e.printStackTrace();
		}
	}

	@Override
	public int getKey() {
		return menuKey;
	}
	
	@Override
	public String toString() {
		return "Load.";
	}
}
