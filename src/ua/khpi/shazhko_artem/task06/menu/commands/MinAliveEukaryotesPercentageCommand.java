package ua.khpi.shazhko_artem.task06.menu.commands;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import ua.khpi.shazhko_artem.task02.biology.FissionSimulation;
import ua.khpi.shazhko_artem.task05.menu.commands.Command;
import ua.khpi.shazhko_artem.task06.views.View;

/**
 * Min alive eukaryotes percentage command
 * 
 * @author Shazhko
 *
 */
public class MinAliveEukaryotesPercentageCommand implements Command {

	private double result = 0.0;
	private int progress = 0;
	private View view;

	/**
	 * getter for ViewResult
	 * 
	 * @return viewResult
	 */
	public View getViewResult() {
		return view;
	}

	/**
	 * setter for ViewResult
	 * 
	 * @param viewResult new viewResult
	 */
	public void setView(View view) {
		this.view = view;
	}

	/**
	 * constructor
	 * 
	 * @param view object for calculating
	 */
	public MinAliveEukaryotesPercentageCommand(View view) {
		this.view = view;
	}

	/**
	 * getter for min result
	 * 
	 * @return average result
	 */
	public double getResult() {
		return result;
	}

	/**
	 * return current state of counting
	 * 
	 * @return is counting is ended
	 */
	public boolean running() {
		return progress < 100;
	}

	@Override
	public void execute() {
		System.out.println("[Min] Start command execution");
		progress = 0;

		ArrayList<FissionSimulation.FissionResult> fissionsResults = view.getFissionsResults();
		int size = fissionsResults.size();

		if (size != 0) {
			result = fissionsResults.get(0).getAliveEukaryotesPercentage();

			for (FissionSimulation.FissionResult fissionResult : fissionsResults) {
				System.out.println("[Min] Processing...");

				if (result > fissionResult.getAliveEukaryotesPercentage())
					result = fissionResult.getAliveEukaryotesPercentage();

				try {
					TimeUnit.MILLISECONDS.sleep(2000 / size);
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
				}
			}
			System.out.println("[Min] Max alive eukaryotes percentage : " + result + " %");
		} else
			System.out.println("[Min] No data.");
		
		progress = 100;
	}

}
