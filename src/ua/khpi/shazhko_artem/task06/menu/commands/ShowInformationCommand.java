package ua.khpi.shazhko_artem.task06.menu.commands;

import ua.khpi.shazhko_artem.task06.views.View;
import ua.khpi.shazhko_artem.task05.menu.commands.ConsoleCommand;

/**
 * Show information command
 * 
 * @author Shazhko
 *
 */
public class ShowInformationCommand implements ConsoleCommand {

	private int menuKey;
	private View view;
	
	public ShowInformationCommand(int key,View view) {
		this.menuKey=key;
		this.view=view;
	}
	@Override
	public void execute() {
		this.view.show();
	}

	@Override
	public int getKey() {
		return menuKey;
	}

	@Override
	public String toString() {
		return "Show information.";
	}
}
