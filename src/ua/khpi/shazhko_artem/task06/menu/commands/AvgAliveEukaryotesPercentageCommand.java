package ua.khpi.shazhko_artem.task06.menu.commands;

import ua.khpi.shazhko_artem.task06.views.View;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import ua.khpi.shazhko_artem.task02.biology.FissionSimulation;
import ua.khpi.shazhko_artem.task05.menu.commands.Command;

/**
 * Command for getting average result from collection
 * 
 * @author Shazhko
 *
 */
public class AvgAliveEukaryotesPercentageCommand implements Command {
	private double result = 0.0;
	private int progress = 0;
	private View view;

	/**
	 * constructor
	 * 
	 * @param view object for calculating
	 */
	public AvgAliveEukaryotesPercentageCommand(View view) {
		this.view = view;
	}

	/**
	 * getter for ViewResult
	 * 
	 * @return viewResult
	 */
	public View getView() {
		return view;
	}

	/**
	 * getter for average result
	 * 
	 * @return average result
	 */
	public double getResult() {
		return result;
	}

	/**
	 * return current state of counting
	 * 
	 * @return is counting is ended
	 */
	public boolean running() {
		return progress < 100;
	}

	@Override
	public void execute() {
		System.out.printf("[Average] Start command execution\n");
		progress = 0;
		result=0;
		
		ArrayList<FissionSimulation.FissionResult> fissionsResults = view.getFissionsResults();
		int size = fissionsResults.size();

		if (size != 0) {
			for (FissionSimulation.FissionResult fissionResult : fissionsResults) {
				System.out.println("[Average] Processing...");
				result += fissionResult.getAliveEukaryotesPercentage();

				try {
					TimeUnit.MILLISECONDS.sleep(5000 / size);
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
				}
			}

			result /= size;
			System.out.println("[Average] Average alive eukaryotes percentage : " + result + " %");
		} else
			System.out.println("[Average] No data.");

		progress = 100;
	}

}
