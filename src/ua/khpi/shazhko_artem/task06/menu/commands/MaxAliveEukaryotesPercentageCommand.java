package ua.khpi.shazhko_artem.task06.menu.commands;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import ua.khpi.shazhko_artem.task02.biology.FissionSimulation;
import ua.khpi.shazhko_artem.task05.menu.commands.Command;
import ua.khpi.shazhko_artem.task06.views.View;

/**
 * Max alive eukaryotes percentage command
 * 
 * @author Shazhko
 *
 */
public class MaxAliveEukaryotesPercentageCommand implements Command {

	private double result = 0.0;
	private int progress = 0;
	private View view;

	/**
	 * getter for view
	 * 
	 * @return view
	 */
	public View getview() {
		return view;
	}

	/**
	 * setter for view
	 * 
	 * @param view new view
	 */
	public void setview(View view) {
		this.view = view;
	}

	/**
	 * constructor
	 * 
	 * @param view object for calculating
	 */
	public MaxAliveEukaryotesPercentageCommand(View view) {
		this.view = view;
	}

	/**
	 * getter for max result
	 * 
	 * @return average result
	 */
	public double getResult() {
		return result;
	}

	/**
	 * return current state of counting
	 * 
	 * @return is counting is ended
	 */
	public boolean running() {
		return progress < 100;
	}

	@Override
	public void execute() {
		System.out.println("[Max] Start command execution");

		progress = 0;
		ArrayList<FissionSimulation.FissionResult> fissionsResults = view.getFissionsResults();
		int size = fissionsResults.size();
		if (size != 0) {
			result = fissionsResults.get(0).getAliveEukaryotesPercentage();

			for (FissionSimulation.FissionResult fissionResult : fissionsResults) {
				System.out.println("[Max] Processing...");
				if (result < fissionResult.getAliveEukaryotesPercentage())
					result = fissionResult.getAliveEukaryotesPercentage();

				try {
					TimeUnit.MILLISECONDS.sleep(3000 / size);
				} catch (InterruptedException e) {
					e.printStackTrace();
					Thread.currentThread().interrupt();
				}
			}
			System.out.println("[Max] Max alive eukaryotes percentage : " + result + " %");
		} else
			System.out.println("[Max] No data.");

		progress = 100;
	}

}
