package ua.khpi.shazhko_artem.task06.menu.commands;

import ua.khpi.shazhko_artem.task06.views.View;
import ua.khpi.shazhko_artem.task05.menu.commands.ConsoleCommand;

/**
 * Initialize environment command
 * 
 * @author Shazhko
 *
 */
public class InitializeEnvironmentCommand implements ConsoleCommand {

	private int menuKey;
	private View view;
	
	public InitializeEnvironmentCommand(int key,View view) {
		this.menuKey=key;
		this.view=view;
	}
	@Override
	public void execute() {
		System.out.println("[Initialize environment] Process....");
		this.view.init();
		System.out.println("[Initialize environment] Done.");
	}

	@Override
	public int getKey() {
		return menuKey;
	}
	
	@Override
	public String toString() {
		return "Initialize environment.";
	}

}
