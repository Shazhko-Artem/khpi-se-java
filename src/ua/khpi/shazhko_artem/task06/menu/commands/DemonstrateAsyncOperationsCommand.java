package ua.khpi.shazhko_artem.task06.menu.commands;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import ua.khpi.shazhko_artem.task05.menu.commands.Command;
import ua.khpi.shazhko_artem.task05.menu.commands.ConsoleCommand;
import ua.khpi.shazhko_artem.task06.views.View;

/**
 * Console command for executing working thread pattern
 * 
 * @author Shazhko
 *
 */
public class DemonstrateAsyncOperationsCommand implements ConsoleCommand {
	private View view;
	private int menuKey;

	/**
	 * getter for View
	 * 
	 * @return view
	 */
	public View getView() {
		return view;
	}

	/**
	 * setter for view
	 * 
	 * @param view new view
	 */
	public void setView(View view) {
		this.view = view;
	}

	/**
	 * constructor
	 * 
	 * @param view object for operating with
	 */
	public DemonstrateAsyncOperationsCommand(int key, View view) {
		this.menuKey = key;
		this.view = view;
	}

	@Override
	public void execute() {
		System.out.println("[Async operations] Start command execution");

		ExecutorService executor = Executors.newCachedThreadPool();
		BlockingQueue<Command> pendingCommands = new LinkedBlockingQueue<>();

		List<Command> plannedCommands1 = Arrays.asList(new MinAliveEukaryotesPercentageCommand(view),
				new MaxAliveEukaryotesPercentageCommand(view));
		
		List<Command> plannedCommands2 = Arrays.asList(new AvgAliveEukaryotesPercentageCommand(view));
		
		CountDownLatch pendingCommandsCount1 = new CountDownLatch(plannedCommands1.size());
		CountDownLatch pendingCommandsCount2 = new CountDownLatch(plannedCommands2.size());

		addToPlans(plannedCommands1,pendingCommands);
		addToPlans(plannedCommands2,pendingCommands);
	

		executor.execute(createCommmandExecutor(pendingCommandsCount1, pendingCommands));
		executor.execute(createCommmandExecutor(pendingCommandsCount2, pendingCommands));

		try {
			pendingCommandsCount1.await();
			pendingCommandsCount2.await();
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}

		executor.shutdownNow();
		System.out.println("[Async operations] Done.");
	}

	private void addToPlans(List<Command> plannedCommands, BlockingQueue<Command> pendingCommands) {
		plannedCommands.forEach(c -> {
			try {
				pendingCommands.put(c);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		});
	}

	private Runnable createCommmandExecutor(final CountDownLatch pendingCommandsCount,
			final BlockingQueue<Command> pendingCommands) {
		return () -> {
			while (pendingCommandsCount.getCount() > 0) {
				try {
					Command command = pendingCommands.take();
					command.execute();
					pendingCommandsCount.countDown();
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
				}
			}
		};
	}

	@Override
	public String toString() {
		return "Demonstrate async operations.";
	}

	@Override
	public int getKey() {
		return this.menuKey;
	}
}
