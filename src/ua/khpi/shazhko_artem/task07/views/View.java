package ua.khpi.shazhko_artem.task07.views;

import java.io.IOException;
import java.util.ArrayList;

import ua.khpi.shazhko_artem.task07.biology.Environment;
import ua.khpi.shazhko_artem.task07.biology.FissionSimulation;

/**
 * fabricated objects interface
 * 
 * @author Daria
 *
 */
public interface View {
	/**
	 * show information about current stage of object
	 */
	public void viewHeader();

	/**
	 * show information about current stage of object
	 */
	public void viewBody();

	/**
	 * show information about current stage of object
	 */
	public void viewFooter();

	/**
	 * show information about current stage of object
	 */
	public void show();

	/**
	 * random initializing
	 */
	public void init();

	public ArrayList<FissionSimulation.FissionResult> getFissionsResults();

	/**
	 * serializing
	 * 
	 * @throws IOException
	 */
	public void save() throws IOException;

	/**
	 * deserializing
	 * 
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public void restore() throws IOException, ClassNotFoundException;

	/**
	 * Set environment
	 * 
	 * @param environment New environment
	 */
	public void setEnvironment(Environment environment);

	/**
	 * Get current environment
	 * 
	 * @return current environment
	 */
	public Environment getEnvironment();
}
