package ua.khpi.shazhko_artem.task07.views;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import ua.khpi.shazhko_artem.task02.FileBasedRepository;
import ua.khpi.shazhko_artem.task02.Repository;
import ua.khpi.shazhko_artem.task07.biology.Environment;
import ua.khpi.shazhko_artem.task07.biology.FissionSimulation;
import ua.khpi.shazhko_artem.task07.biology.FissionSimulation.FissionResult;

public class ViewTable implements View {
	/**
	 * number of elements in collection
	 */
	public static final int DEFAULT_QUANTITY_STEPS = 3;
	/**
	 * file for storing information
	 */
	public static final String FILE_NAME = "task03.viewResult.ini";
	public static final int DEFAULT_EUKARYOTE_QUANTITY = 100;
	/**
	 * collection of "Store" object
	 */
	ArrayList<FissionSimulation.FissionResult> fissionResults;
	private int quantitySteps;
	private Repository repository;

	private Environment environment;

	/**
	 * default constructor
	 */
	public ViewTable() {
		fissionResults = new ArrayList<>();
		repository = new FileBasedRepository(FILE_NAME);
		quantitySteps = DEFAULT_QUANTITY_STEPS;
		environment = new Environment();
	}

	public ViewTable(int quantitySteps) {
		this.quantitySteps = quantitySteps;
		fissionResults = new ArrayList<>();
		repository = new FileBasedRepository(FILE_NAME);
		environment = new Environment();
	}

	/**
	 * random initialization
	 */
	public void init() {
		environment.init(DEFAULT_EUKARYOTE_QUANTITY);
		for (int i = 0; i < this.quantitySteps; i++) {
			fissionResults.add(environment.binaryFissionEukaryotes());
		}
	}
	
	public void setEnvironment(Environment environment) {
		fissionResults.clear();
		this.environment=environment;
	}
	
	public Environment getEnvironment() {
		return this.environment;
	}

	@Override
	public void viewHeader() {
		System.out.format("+----------------------------------------------+%n");
		System.out.format("| %-44s |%n", "Environment");
		System.out.format("+--------------------+-------------------------+%n");
		String leftAlignFormat = "| %-18s | %-23s |%n";

		System.out.format(leftAlignFormat, "Eukaryote quantity","Is favorable conditions");
		System.out.format("+--------------------+-------------------------+%n");
		System.out.format(leftAlignFormat,  environment.getEukaryoteQuantity(), environment.isFavorableConditions());

		System.out.format("+--------------------+-------------------------+%n");
	}

	@Override
	public void viewBody() {
		System.out.format(
				"+---------------------------------------------------------------------------------------------+%n");
		System.out.format("| %-91s |%n", "Binary fissione ukaryotes");
		System.out.format(
				"+------+------------+------------------+-----------------+------------------+-----------------+%n");

		String leftAlignFormat = "| %-4s | %-10s | %-16s | %-15s | %-16s | %-15s |%n";
		System.out.format(leftAlignFormat, "Step", "Eukaryotes", "Alive eukaryotes", "Dead eukaryotes",
				"Alive eukaryotes", "Dead eukaryotes");
		System.out.format(leftAlignFormat, "", "count", "percentage", "percentage", "count", "count");
		System.out.format(
				"+------+------------+------------------+-----------------+------------------+-----------------+%n");

		for (FissionSimulation.FissionResult fissionResult : fissionResults) {
			System.out.format(leftAlignFormat, "# " + fissionResult.getNumber(), fissionResult.getEukaryotesCount(),
					fissionResult.getAliveEukaryotesPercentage() + " %",
					fissionResult.getDeadEukaryotesPercentage() + " %", fissionResult.getAliveEukaryotesCount(),
					fissionResult.getDeadEukaryotesCount());
			System.out.format(
					"+------+------------+------------------+-----------------+------------------+-----------------+%n");
		}
	}

	@Override
	public void viewFooter() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		System.out.format("| %-91s |%n", "Date : " + dtf.format(now));
		System.out.format(
				"+---------------------------------------------------------------------------------------------+%n");
	}

	@Override
	public void show() {
		viewHeader();
		viewBody();
		viewFooter();
	}

	@Override
	public void save() throws IOException {
		repository.save(fissionResults);
	}

	/**
	 * deserialization
	 */
	@SuppressWarnings("unchecked")
	public void restore() throws IOException, ClassNotFoundException {
		fissionResults = (ArrayList<FissionSimulation.FissionResult>) repository.load();
	}
	
	@Override
	public ArrayList<FissionResult> getFissionsResults() {
		return new ArrayList<>(this.fissionResults);
	}
}
