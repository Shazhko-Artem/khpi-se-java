package ua.khpi.shazhko_artem.task07.views;

/**
 * fabricating object interface
 * @author Daria
 *
 */
public interface Viewable {
	/**
	 * fabricate view
	 * @return fabricated object
	 */
	View getView();
}
