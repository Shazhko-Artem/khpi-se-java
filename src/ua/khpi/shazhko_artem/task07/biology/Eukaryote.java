package ua.khpi.shazhko_artem.task07.biology;



import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;

public class Eukaryote implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private boolean is_alive;
	private int number;
	
	
	public Eukaryote() {
		is_alive=true;
	}
	public Eukaryote(int number){
		is_alive=true;
		this.number=number;
	}
	
	public boolean isAlive() {return is_alive;}
	
	public int getNumber() {return number;}
	
	public void die() {is_alive=false;}
	
	public Collection<Eukaryote>  binaryFission() {
		 LinkedList<Eukaryote> eukaryotes=new LinkedList<>();
		 eukaryotes.add(new Eukaryote((int)(Math.random()*1000)));
		 eukaryotes.add(new Eukaryote((int)(Math.random()*1000)));
		 
		 return eukaryotes;
	}
}
