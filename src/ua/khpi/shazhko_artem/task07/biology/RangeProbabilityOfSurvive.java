package ua.khpi.shazhko_artem.task07.biology;

import java.io.Serializable;

public class RangeProbabilityOfSurvive implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2920772563475475233L;
	private double minProbabilityOfSurvive;
	private double maxProbabilityOfSurvive;

	public RangeProbabilityOfSurvive(double minProbabilityOfSurvive,double maxProbabilityOfSurvive) {
		this.minProbabilityOfSurvive=minProbabilityOfSurvive;
		this.maxProbabilityOfSurvive=maxProbabilityOfSurvive;
	}
	
	public double getMinProbabilityOfSurvive() {
		return this.minProbabilityOfSurvive;
	}

	public void setMinProbabilityOfSurvive(double minProbabilityOfSurvive) {
		this.minProbabilityOfSurvive = minProbabilityOfSurvive;
	}

	public double getMaxProbabilityOfSurvive() {
		return this.maxProbabilityOfSurvive;
	}


	public void setMaxProbabilityOfSurvive(double maxProbabilityOfSurvive) {
		this.maxProbabilityOfSurvive = maxProbabilityOfSurvive;
	}

	public double randomizeProbabilityOfSurvive() {
		return ua.khpi.shazhko_artem.task02.math.Util.getRandomDoubleBetweenRange(this.minProbabilityOfSurvive,
				this.maxProbabilityOfSurvive);
	}
}
