package ua.khpi.shazhko_artem.task07.biology;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class FissionSimulation implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6294268819228506409L;

	public class FissionResult implements Serializable{
		/**
		 * 
		 */
		private static final long serialVersionUID = -311117750549703559L;
		private int number;
		private Collection<Eukaryote> aliveEukaryote;
		private Collection<Eukaryote> deadEukaryote;

		public FissionResult(int number, Collection<Eukaryote> eukaryotes) {
			aliveEukaryote = eukaryotes.stream().filter(new Predicate<Eukaryote>() {
				@Override
				public boolean test(Eukaryote eukaryote) {
					return eukaryote.isAlive();
				}
			}).collect(Collectors.toList());
			this.number = number;

			deadEukaryote = eukaryotes.stream().filter(new Predicate<Eukaryote>() {
				@Override
				public boolean test(Eukaryote eukaryote) {
					return !eukaryote.isAlive();
				}
			}).collect(Collectors.toList());

			this.number = number;
		}

		public int getNumber() {
			return number;
		}

		public int getAliveEukaryotesCount() {
			return aliveEukaryote.size();
		}

		public int getDeadEukaryotesCount() {
			return deadEukaryote.size();
		}

		public int getEukaryotesCount() {
			return getAliveEukaryotesCount() + getDeadEukaryotesCount();
		}

		public double getAliveEukaryotesPercentage() {
			if (getAliveEukaryotesCount() < 1)
				return 0;

			double percentage = ((double)getAliveEukaryotesCount() / getEukaryotesCount()) * 100;
			return Math.round(percentage * 100.0) / 100.0;
		}

		public double getDeadEukaryotesPercentage() {
			if (getDeadEukaryotesCount() < 1)
				return 0;

			double percentage = ((double)getDeadEukaryotesCount() / getEukaryotesCount()) * 100;
			return Math.round(percentage * 100.0) / 100.0;
		}
	}

	private int currentStep = 0;
	private RangeProbabilityOfSurvive favorableConditions;
	private RangeProbabilityOfSurvive notFavorableConditions;

	public FissionSimulation(RangeProbabilityOfSurvive favorableConditions,
			RangeProbabilityOfSurvive notFavorableConditions) {
		this.favorableConditions = favorableConditions;
		this.notFavorableConditions = notFavorableConditions;
	}


	public void simulateEukaryotesDeath(Environment environment, Collection<Eukaryote> eukaryotes) {
		double probabilityOfDeath;
		if (environment.isFavorableConditions())
			probabilityOfDeath =1 -favorableConditions.randomizeProbabilityOfSurvive();
		else
			probabilityOfDeath =1- notFavorableConditions.randomizeProbabilityOfSurvive();

		int deadEukaryotesCount = (int) (eukaryotes.size() * probabilityOfDeath);

		for (Eukaryote eukaryote : eukaryotes) {
			if (deadEukaryotesCount > 0)
				eukaryote.die();
			deadEukaryotesCount--;
		}
	}

	public FissionResult simulateEukaryotesFission(Environment environment, Collection<Eukaryote> eukaryotes) {
		LinkedList<Eukaryote> newGeneration = new LinkedList<>();
		for (Eukaryote eukaryote : eukaryotes) {
			for (Eukaryote childEukaryote : eukaryote.binaryFission()) {
				newGeneration.push(childEukaryote);
			}
		}

		simulateEukaryotesDeath(environment, newGeneration);
		eukaryotes.clear();
		eukaryotes.addAll(newGeneration);
		
		currentStep++;
		return new FissionSimulation.FissionResult(currentStep, newGeneration);
	}
}
