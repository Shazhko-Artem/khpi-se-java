package ua.khpi.shazhko_artem.task07.biology.logger;

import ua.khpi.shazhko_artem.task07.AnnotatedObserver;
import ua.khpi.shazhko_artem.task07.Event;
import ua.khpi.shazhko_artem.task07.biology.Environment;

/* 
 * Clean dead eukaryots logger
* @author Shazhko
*/
public class CleanDeadEukaryotsLogger extends AnnotatedObserver {

	@Event(Environment.ON_CLEANING_DEAD_EUKARYOTS)
	public void onCleaningDeadEukaryots(Environment environment) {
		System.out.println("[CleanDeadEukaryotsLogger] Eukaryote quantity before clean dead eukaryote : "+environment.getEukaryoteQuantity());
	}
	
	@Event(Environment.ON_CLEANED_DEAD_EUKARYOTS)
	public void onCleanedDeadEukaryots(Environment environment) {
		System.out.println("[CleanDeadEukaryotsLogger] Eukaryote quantity after clean dead eukaryote : "+environment.getEukaryoteQuantity());
	}
}
