package ua.khpi.shazhko_artem.task07.biology.logger;

import ua.khpi.shazhko_artem.task07.AnnotatedObserver;
import ua.khpi.shazhko_artem.task07.Event;
import ua.khpi.shazhko_artem.task07.biology.Environment;

/* 
 * Initialize logger
* @author Shazhko
*/
public class InitializeLogger extends AnnotatedObserver {

	@Event(Environment.ON_INITIALIZING)
	public void onInitializing(Environment environment) {
		System.out.println("[InitializeLogger] Begin initialization.");
	}
	
	@Event(Environment.ON_INITIALIZED)
	public void onInitialized(Environment environment) {
		System.out.println("[InitializeLogger] End initialization.");
	}
}
