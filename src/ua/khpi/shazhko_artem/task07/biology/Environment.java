package ua.khpi.shazhko_artem.task07.biology;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import ua.khpi.shazhko_artem.task02.FileBasedRepository;
import ua.khpi.shazhko_artem.task02.Repository;
import ua.khpi.shazhko_artem.task07.Observable;
import ua.khpi.shazhko_artem.task07.biology.FissionSimulation.FissionResult;


/* 
 * Environment
* @author Shazhko
*/
public class Environment extends Observable implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8934555379681849253L;
	private boolean isFavorableConditions;
	private FissionSimulation fissionSimulation;
	private transient Repository repository = new FileBasedRepository("environment.ini");
	private Collection<Eukaryote> eukaryotes = new LinkedList<Eukaryote>();

	public static final String ON_CLEANING_DEAD_EUKARYOTS = "ON_CLEANING_DEAD_EUKARYOTS";
	public static final String ON_CLEANED_DEAD_EUKARYOTS = "ON_CLEANED_DEAD_EUKARYOTS";
	public static final String ON_BINARY_FISSION_EUKARYOTES = "ON_BINARY_FISSION_EUKARYOTES";
	public static final String ON_INITIALIZING = "ON_INITIALIZING";
	public static final String ON_INITIALIZED = "ON_INITIALIZED";
	public static final String ON_SAVING = "ON_SAVING";
	public static final String ON_SAVED = "ON_SAVED";
	public static final String ON_LOADING = "ON_LOADING";
	public static final String ON_LOADED = "ON_LOADED";

	public int getEukaryoteQuantity() {
		return eukaryotes.size();
	}

	public boolean isFavorableConditions() {
		return this.isFavorableConditions;
	}

	public Environment() {
		this.fissionSimulation = new FissionSimulation(new RangeProbabilityOfSurvive(0.5, 0.9),
				new RangeProbabilityOfSurvive(0.1, 0.5));
	}

	public Environment(Repository repository) {
		this.repository = repository;
		this.fissionSimulation = new FissionSimulation(new RangeProbabilityOfSurvive(0.5, 0.9),
				new RangeProbabilityOfSurvive(0.1, 0.5));
	}

	public Environment(boolean isFavorableConditions) {
		this.isFavorableConditions = isFavorableConditions;
		this.fissionSimulation = new FissionSimulation(new RangeProbabilityOfSurvive(0.5, 0.9),
				new RangeProbabilityOfSurvive(0.1, 0.5));
	}

	public Environment(Repository repository, boolean isFavorableConditions) {
		this.repository = repository;
		this.isFavorableConditions = isFavorableConditions;
		this.fissionSimulation = new FissionSimulation(new RangeProbabilityOfSurvive(0.5, 0.9),
				new RangeProbabilityOfSurvive(0.1, 0.5));
	}

	public FissionResult binaryFissionEukaryotes() {
		FissionResult fissionResult = fissionSimulation.simulateEukaryotesFission(this, eukaryotes);
		cleanDeadEukaryots();
		call(ON_BINARY_FISSION_EUKARYOTES);
		return fissionResult;
	}

	public void init(int eukaryoteCount) {
		call(ON_INITIALIZING);
		eukaryotes.clear();
		for (int i = 0; i < eukaryoteCount; i++)
			eukaryotes.add(new Eukaryote(i));

		call(ON_INITIALIZED);
	}

	public void save() throws IOException {
		call(ON_SAVING);
		repository.save(this);
		call(ON_SAVED);
	}

	public void load() throws ClassNotFoundException, IOException {
		call(ON_LOADING);
		Object object = repository.load();
		Environment env = (Environment) object;
		this.eukaryotes = env.eukaryotes;
		this.fissionSimulation = env.fissionSimulation;
		this.isFavorableConditions = env.isFavorableConditions;
		call(ON_LOADED);
	}

	public void cleanDeadEukaryots() {
		call(ON_CLEANING_DEAD_EUKARYOTS);

		eukaryotes = eukaryotes.stream().filter(new Predicate<Eukaryote>() {
			@Override
			public boolean test(Eukaryote eukaryote) {
				return eukaryote.isAlive();
			}
		}).collect(Collectors.toList());
		call(ON_CLEANED_DEAD_EUKARYOTS);

	}
}
