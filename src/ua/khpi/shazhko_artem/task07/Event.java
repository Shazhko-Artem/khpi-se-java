package ua.khpi.shazhko_artem.task07;

import java.lang.annotation.*;

/**
 * Runtime annotation for appointment to Observer methods
 * 
 * @author Shazhko
 *
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Event {
	public String value();
}
