package ua.khpi.shazhko_artem.task07;

import java.util.HashSet;
import java.util.Set;

/**
 * Defines the interaction of observer and observable
 * 
 * @author Daria
 *
 */
public abstract class Observable {
	/**
	 * set of observers
	 */
	private Set<Observer> observers = new HashSet<Observer>();

	/**
	 * add observer
	 * 
	 * @param observer
	 *            observer object
	 */
	public void addObserver(Observer observer) {
		observers.add(observer);
	}

	/**
	 * delete observer
	 * 
	 * @param observer
	 *            object observer
	 */
	public void delObserver(Observer observer) {
		observers.remove(observer);
	}

	/**
	 * Call observers about event
	 * 
	 * @param event
	 *            information about event
	 */
	public void call(Object event) {
		for (Observer observer : observers) {
			observer.HandleEvent(this, event);
		}
	}
}
