package ua.khpi.shazhko_artem.task07;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Basic observer class, use annotation for defining methods, which process
 * different events
 * 
 * @author Shazhko
 *
 */
public class AnnotatedObserver implements Observer {
	/**
	 * includes pairs - event and its process
	 */
	private Map<Object, Method> handlers = new HashMap<Object, Method>();

	/**
	 * fills map by links on methods, which underlined by annotation
	 */
	public AnnotatedObserver() {
		for (Method m : this.getClass().getMethods()) {
			if (m.isAnnotationPresent(Event.class)) {
				handlers.put(m.getAnnotation(Event.class).value(), m);
			}
		}
	}

	@Override
	public void HandleEvent(Observable observable, Object event) {
		Method m = handlers.get(event);
		if (m != null)
			try {
				m.setAccessible(true);
				m.invoke(this, observable);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			}

	}

}
