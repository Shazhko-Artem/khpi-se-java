package ua.khpi.shazhko_artem.task07.menu.commands;

import ua.khpi.shazhko_artem.task07.views.View;
import ua.khpi.shazhko_artem.task05.menu.commands.ConsoleCommand;

public class ShowInformationCommand implements ConsoleCommand {

	private int menuKey;
	private View view;
	
	public ShowInformationCommand(int key,View view) {
		this.menuKey=key;
		this.view=view;
	}
	@Override
	public void execute() {
		this.view.show();
	}

	@Override
	public int getKey() {
		return menuKey;
	}

	@Override
	public String toString() {
		return "Show information.";
	}
}
