package ua.khpi.shazhko_artem.task07.menu.commands;

import java.io.IOException;

import ua.khpi.shazhko_artem.task05.menu.commands.ConsoleCommand;
import ua.khpi.shazhko_artem.task07.views.View;

public class SaveCommand implements ConsoleCommand {
	private int menuKey;
	private View view;
	
	public SaveCommand(int key,View view) {
		this.menuKey=key;
		this.view=view;
	}
	@Override
	public void execute() {
		System.out.println("[Save] Process...");
		try {
			view.save();
			System.out.println("[Save] Done.");
		} catch (IOException e) {
			System.out.println("[ERROR] " + e.getMessage());
			e.printStackTrace();
		}
	}

	@Override
	public int getKey() {
		return menuKey;
	}
	
	@Override
	public String toString() {
		return "Save.";
	}
}
