package ua.khpi.shazhko_artem.task07.menu.commands;

import ua.khpi.shazhko_artem.task07.views.View;
import ua.khpi.shazhko_artem.task05.menu.commands.ConsoleCommand;

public class InitializeEnvironmentCommand implements ConsoleCommand {

	private int menuKey;
	private View view;
	
	public InitializeEnvironmentCommand(int key,View view) {
		this.menuKey=key;
		this.view=view;
	}
	@Override
	public void execute() {
		System.out.println("[Initialize environment] Process....");
		this.view.init();
		System.out.println("[Initialize environment] Done.");
	}

	@Override
	public int getKey() {
		return menuKey;
	}
	
	@Override
	public String toString() {
		return "Initialize environment.";
	}

}
