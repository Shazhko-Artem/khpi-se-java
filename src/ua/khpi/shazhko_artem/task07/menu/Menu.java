package ua.khpi.shazhko_artem.task07.menu;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

import ua.khpi.shazhko_artem.task05.menu.commands.Command;
import ua.khpi.shazhko_artem.task05.menu.commands.ConsoleCommand;

public class Menu implements Command {

	private List<ConsoleCommand> menuCommands = new ArrayList<ConsoleCommand>();
	private int exitKey = 1;

	/**
	 * add command
	 * 
	 * @param command new command
	 */
	public void add(ConsoleCommand command) {
		menuCommands.add(command);
	}
	
	public void clear() {
		menuCommands.clear();;
	}
	
	public String toString() {
		String result =	String.format("+-------------------------------------+%n"
					+	"|              Menu                   |%n"
					+	"+-------------------------------------+%n");

		String format = "| %-35s |%n";
		menuCommands.sort(new Comparator<ConsoleCommand>() {
			@Override
			public int compare(ConsoleCommand o1, ConsoleCommand o2) {
				return Integer.compare(o1.getKey(), o2.getKey());
			}
		});

		for (ConsoleCommand c : menuCommands) {
			result += String.format(format, c.getKey() + ") " + c);
		}
		exitKey = menuCommands.size()+1;

		result += String.format(format, exitKey + ") Exit.");
		result+="+-------------------------------------+";
		return result;
	}

	@Override
	public void execute() {
		int key;
		try (Scanner scanners = new Scanner(System.in)) {
			while (true) {
				System.out.println(toString());
				System.out.print(">>> ");
				key = scanners.nextInt();
				if (key == exitKey){
					System.out.println("[Exit] Done.");
					break;
				}
				for (ConsoleCommand command : menuCommands) {
					if (key == command.getKey()) {
						command.execute();
					}
				}
			}
		} catch (Exception e) {
			System.out.println("[ERROR] " + e.getMessage());
			e.printStackTrace();
		}

	}

}
