package ua.khpi.shazhko_artem.task02.biology;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.IOException;

import org.junit.jupiter.api.Test;

import ua.khpi.shazhko_artem.task02.FileBasedRepository;

class EnvironmentTest {

	@Test
	void ctor_CreateEnvironment_WithParameter() {
		Environment environment=new Environment(true);
		assertNotNull(environment);
	}
	
	@Test
	void ctor_CreateEnvironment_WithoutParameter() {
		Environment environment=new Environment();
		assertNotNull(environment);
	}
	
	@Test
	void isFavorableConditions_True_CreateEnvironmentWithParameterTrue() {
		boolean expectedIsFavorableConditions=true;
		Environment environment=new Environment(expectedIsFavorableConditions);

		boolean actualIsFavorableConditions=environment.isFavorableConditions();
		assertEquals(expectedIsFavorableConditions, actualIsFavorableConditions);
	}
	
	@Test
	void isFavorableConditions_False_CreateEnvironmentWithParameterFalse() {
		boolean expectedIsFavorableConditions=false;
		Environment environment=new Environment(expectedIsFavorableConditions);

		boolean actualIsFavorableConditions=environment.isFavorableConditions();
		assertEquals(expectedIsFavorableConditions, actualIsFavorableConditions);
	}
	
	@Test
	void getEukaryoteQuatity_0_AfterCreateEnvironment() {
		Environment environment=new Environment();

		int expectedCount=0;
		int actualCount=environment.getEukaryoteQuantity();
		assertEquals(expectedCount, actualCount);
	}
	@Test
	void init_5ofEukaryote_AfterGetEukaryoteQuatity() {
		Environment environment=new Environment();

		int expectedCount=5;
		environment.init(expectedCount);
		
		int actualCount=environment.getEukaryoteQuantity();
		assertEquals(expectedCount, actualCount);
	}
	
	@Test
	void save_SerializeData_AffterInit() throws IOException {
		Environment environment=new Environment(new FileBasedRepository("task02.test.environment.ini"));

		environment.init(5);
		
		environment.save();
		File file = new File("task02.test.environment.ini");
		
		long actualSize=file.length();
		long expectedSize=764;
		
		assertEquals(expectedSize, actualSize);
	}
	
	@Test
	void load_5ofEukaryote_AfterGetEukaryoteQuatity() throws IOException, ClassNotFoundException {
		Environment environment=new Environment(new FileBasedRepository("task02.test.environment.ini"));
		environment.init(5);
		environment.save();
		
		environment=new Environment(new FileBasedRepository("task02.test.environment.ini"));
		
		environment.load();
		
		long actualEukaryoteQuatity=environment.getEukaryoteQuantity();
		long expectedEukaryoteQuatity=5;
		
		assertEquals(expectedEukaryoteQuatity, actualEukaryoteQuatity);
	}
	
	@Test
	void binaryFissionEukaryotes_EukaryoteQuatityInRanchFrom10To50PercentOfSurvive_NonFavorableConditions() {
		Environment environment=new Environment(false);
		int eukaryoteQuatity=50;
		environment.init(eukaryoteQuatity);
		eukaryoteQuatity*=2;
		
		environment.binaryFissionEukaryotes();
		
		int actualEukaryoteQuatity=environment.getEukaryoteQuantity();
		int expectedEukaryoteQuatityFrom=(int)(eukaryoteQuatity*0.1);
		int expectedEukaryoteQuatityTo=(int)(eukaryoteQuatity*0.5);
		
		assertTrue(actualEukaryoteQuatity>=expectedEukaryoteQuatityFrom
				&& actualEukaryoteQuatity<=expectedEukaryoteQuatityTo);
	}
	
	@Test
	void binaryFissionEukaryotes_EukaryoteQuatityInRanchFrom10To50PercentOfSurvive_FavorableConditions() {
		Environment environment=new Environment(true);
		int eukaryoteQuatity=50;
		environment.init(eukaryoteQuatity);
		eukaryoteQuatity*=2;
		
		environment.binaryFissionEukaryotes();
		
		int actualEukaryoteQuatity=environment.getEukaryoteQuantity();
		int expectedEukaryoteQuatityFrom=(int)(eukaryoteQuatity*0.5);
		int expectedEukaryoteQuatityTo=(int)(eukaryoteQuatity*0.9);
		
		assertTrue(actualEukaryoteQuatity>=expectedEukaryoteQuatityFrom
				&& actualEukaryoteQuatity<=expectedEukaryoteQuatityTo);
	}
}
