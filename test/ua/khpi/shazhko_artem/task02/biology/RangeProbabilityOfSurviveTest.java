package ua.khpi.shazhko_artem.task02.biology;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class RangeProbabilityOfSurviveTest {

	@Test
	void getMinProbabilityOfSurvive_10Percent_CreateRangeProbabilityOfSurviveWithParam10and50Percent() {
		double expectMinProbabilityOfSurvive=0.10;
		RangeProbabilityOfSurvive rangeProbabilityOfSurvive=
				new RangeProbabilityOfSurvive(expectMinProbabilityOfSurvive, 0.50);
		
		double actualMinProbabilityOfSurvive=rangeProbabilityOfSurvive.getMinProbabilityOfSurvive();
		assertEquals(expectMinProbabilityOfSurvive, actualMinProbabilityOfSurvive);
	}
	
	@Test
	void getMaxProbabilityOfSurvive_50Percent_CreateRangeProbabilityOfSurviveWithParam10and50Percent() {
		double expectMaxProbabilityOfSurvive=0.50;
		RangeProbabilityOfSurvive rangeProbabilityOfSurvive=
				new RangeProbabilityOfSurvive(0.10,expectMaxProbabilityOfSurvive);
		
		double actualMaxProbabilityOfSurvive=rangeProbabilityOfSurvive.getMaxProbabilityOfSurvive();
		assertEquals(expectMaxProbabilityOfSurvive, actualMaxProbabilityOfSurvive);
	}
	
	@Test
	void setMinProbabilityOfSurvive_10Percent_GetMinProbabilityOfSurvive() {
		RangeProbabilityOfSurvive rangeProbabilityOfSurvive=
				new RangeProbabilityOfSurvive(0, 0);
		
		double expectMinProbabilityOfSurvive=0.10;

		rangeProbabilityOfSurvive.setMinProbabilityOfSurvive(expectMinProbabilityOfSurvive);
		
		double actualMinProbabilityOfSurvive=rangeProbabilityOfSurvive.getMinProbabilityOfSurvive();
		assertEquals(expectMinProbabilityOfSurvive, actualMinProbabilityOfSurvive);
	}
	
	@Test
	void setMaxProbabilityOfSurvive_50Percent_GetMaxProbabilityOfSurvive() {
		RangeProbabilityOfSurvive rangeProbabilityOfSurvive=
				new RangeProbabilityOfSurvive(0,0);
		
		double expectMaxProbabilityOfSurvive=0.50;
		rangeProbabilityOfSurvive.setMaxProbabilityOfSurvive(expectMaxProbabilityOfSurvive);

		double actualMaxProbabilityOfSurvive=rangeProbabilityOfSurvive.getMaxProbabilityOfSurvive();
		assertEquals(expectMaxProbabilityOfSurvive, actualMaxProbabilityOfSurvive);
	}
	
	@Test
	void randomizeProbabilityOfSurvive_Between10And50Percent_Min10AndMax50() {
		double expectaxMinProbabilityOfSurvive=0.10;
		double expectaxMaxProbabilityOfSurvive=0.50;
		
		RangeProbabilityOfSurvive rangeProbabilityOfSurvive=
				new RangeProbabilityOfSurvive(expectaxMinProbabilityOfSurvive,expectaxMaxProbabilityOfSurvive);
		
		double actualProbabilityOfSurvive=rangeProbabilityOfSurvive.randomizeProbabilityOfSurvive();
		assertTrue(actualProbabilityOfSurvive<=expectaxMaxProbabilityOfSurvive&&
				actualProbabilityOfSurvive>=expectaxMinProbabilityOfSurvive);
	}

}
