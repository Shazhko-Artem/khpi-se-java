package ua.khpi.shazhko_artem.task02.biology;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Collection;

import org.junit.jupiter.api.Test;

class EukaryoteTest {

	@Test
	void ctor_CreateEukaryote_WithParameter() {
		Eukaryote eukaryote=new Eukaryote(5);
		assertNotNull(eukaryote);
	}
	@Test
	void ctor_CreateEukaryote_WithoutParameter() {
		Eukaryote eukaryote=new Eukaryote();
		assertNotNull(eukaryote);
	}
	@Test
	void getNumber_Get5_CreateEukaryoteWithParameter5() {
		int expectNumber=5;
		Eukaryote eukaryote=new Eukaryote(expectNumber);

		int actualNumber=eukaryote.getNumber();

		assertEquals(expectNumber, actualNumber);
	}
	@Test
	void isAlive_True_CreateEukaryote() {
		Eukaryote eukaryote=new Eukaryote();
		
		boolean expectedState=true;
		boolean actualState=eukaryote.isAlive();
		
		assertEquals(expectedState, actualState);
	}
	
	@Test
	void die_IsAliveFalse_InvokeDie() {
		Eukaryote eukaryote=new Eukaryote();
		
		eukaryote.die();
		
		boolean expectedState=false;
		boolean actualState=eukaryote.isAlive();
		
		assertEquals(expectedState, actualState);
	}
	
	@Test
	void binaryFission_GetTwoEukaryotes() {
		Eukaryote eukaryote=new Eukaryote();
		Collection<Eukaryote> eukaryotes =eukaryote.binaryFission();
		assertNotNull(eukaryotes);
		int expectedSize=2;
		int actualSize=eukaryotes.size();
		assertEquals(expectedSize, actualSize);
	}
}
