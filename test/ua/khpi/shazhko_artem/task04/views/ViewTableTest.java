package ua.khpi.shazhko_artem.task04.views;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import ua.khpi.shazhko_artem.task02.FileBasedRepository;
import ua.khpi.shazhko_artem.task02.Repository;
import ua.khpi.shazhko_artem.task02.biology.FissionSimulation;

class ViewTableTest {

	@SuppressWarnings("unchecked")
	@Test
	public void save_getSavedFissionResult_affterInit() throws IOException, ClassNotFoundException {
		String fileName="task04.views.viewResultTest.ini";
		Repository repository=new FileBasedRepository(fileName);

		int expectedFissionResultsSize=5;
		
		ViewTable view=new ViewTable(repository,expectedFissionResultsSize);
		view.init();
		
		view.save();

		ArrayList<FissionSimulation.FissionResult> fissionResults=(ArrayList<FissionSimulation.FissionResult>)repository.load();
		
		assertNotNull(fissionResults);
		
		int actualFissionResultsSize= fissionResults.size();
		assertEquals(expectedFissionResultsSize,actualFissionResultsSize);
	}

}
