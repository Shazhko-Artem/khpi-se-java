package ua.khpi.shazhko_artem.task04.views;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ua.khpi.shazhko_artem.task03.views.View;

class ViewableTableTest {

	@Test
	void getView_ViewResultObject() {
		ViewableTable viewableTable=new ViewableTable();
		View view=viewableTable.getView();
		
		assertTrue(view instanceof  ViewTable);
	}

}
