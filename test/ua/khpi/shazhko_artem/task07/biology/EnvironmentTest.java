package ua.khpi.shazhko_artem.task07.biology;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class EnvironmentTest {

	@Test
	void binaryFissionEukaryotesEvent_NotificationToSubscribers_AffterBinaryFissionEukaryotes() {
		int quantity = 100;
		Environment environment = new Environment(false);
		environment.init(quantity);

		int expectFrom = (int) (quantity * 0.1);
		int expectTo = (int) (quantity * 0.5);
		BinaryFissionEukaryotesLoggerMock mock = new BinaryFissionEukaryotesLoggerMock(expectFrom, expectTo);
		environment.addObserver(mock);

		environment.binaryFissionEukaryotes();

		assertTrue(mock.isNotified());
	}

	@Test
	void cleanDeadEukaryotsEvent_NotificationToSubscribers_BeforeAndAffterCleanDeadEukaryots() {
		int quantity = 100;
		Environment environment = new Environment(false);
		environment.init(quantity);

		CleanDeadEukaryotsMock mock = new CleanDeadEukaryotsMock();
		environment.addObserver(mock);

		environment.binaryFissionEukaryotes();

		assertTrue(mock.isNotifiedCleaning());
		assertTrue(mock.isNotifiedCleaned());
	}

	@Test
	void initializeEvent_NotificationToSubscribers_BeforeAndAffterInitialized() {
		int quantity = 100;
		Environment environment = new Environment(false);

		InitializeMock mock = new InitializeMock();
		environment.addObserver(mock);

		environment.init(quantity);

		assertTrue(mock.isNotifiedInitializing());
		assertTrue(mock.isNotifiedInitialized());
	}
}