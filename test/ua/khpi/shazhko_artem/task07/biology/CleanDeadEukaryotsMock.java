package ua.khpi.shazhko_artem.task07.biology;

import static org.junit.Assert.assertTrue;

import ua.khpi.shazhko_artem.task07.AnnotatedObserver;
import ua.khpi.shazhko_artem.task07.Event;

public class CleanDeadEukaryotsMock extends AnnotatedObserver {

	private boolean isNotifiedCleaning;
	private boolean isNotifiedCleaned;
	private int eukaryoteQuantit;
	
	
	public boolean isNotifiedCleaning() {
		return isNotifiedCleaning;
	}

	public boolean isNotifiedCleaned() {
		return isNotifiedCleaned;
	}

	@Event(Environment.ON_CLEANING_DEAD_EUKARYOTS)
	public void onCleaningDeadEukaryots(Environment environment) {
		eukaryoteQuantit=environment.getEukaryoteQuantity();
		isNotifiedCleaning = true;
	}

	@Event(Environment.ON_CLEANED_DEAD_EUKARYOTS)
	public void onCleanedDeadEukaryots(Environment environment) {
		isNotifiedCleaned = true;
		assertTrue(eukaryoteQuantit>environment.getEukaryoteQuantity());
	}
}
