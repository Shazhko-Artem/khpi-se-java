package ua.khpi.shazhko_artem.task07.biology;

import static org.junit.Assert.assertTrue;
import ua.khpi.shazhko_artem.task07.AnnotatedObserver;
import ua.khpi.shazhko_artem.task07.Event;

public class BinaryFissionEukaryotesLoggerMock extends AnnotatedObserver {
	private boolean isNotified;
	private int expectFrom;
	private int expectTo;

	public boolean isNotified() {
		return isNotified;
	}

	public BinaryFissionEukaryotesLoggerMock(int expectFrom, int expectTo) {
		this.expectFrom = expectFrom;
		this.expectTo = expectTo;
	}

	@Event(Environment.ON_BINARY_FISSION_EUKARYOTES)
	public void onBinaryFissionEukaryotesLogger(Environment environment) {
		double actual = environment.getEukaryoteQuantity();
		isNotified = true;
		assertTrue(actual>=expectFrom && actual<=expectTo);
	}
}
