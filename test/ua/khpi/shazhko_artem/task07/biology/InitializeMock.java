package ua.khpi.shazhko_artem.task07.biology;

import ua.khpi.shazhko_artem.task07.AnnotatedObserver;
import ua.khpi.shazhko_artem.task07.Event;

public class InitializeMock extends AnnotatedObserver {

	private boolean isNotifiedInitializing;
	private boolean isNotifiedInitialized;

	public boolean isNotifiedInitializing() {
		return isNotifiedInitializing;
	}
	
	public boolean isNotifiedInitialized() {
		return isNotifiedInitialized;
	}

	@Event(Environment.ON_INITIALIZING)
	public void onInitializing(Environment environment) {
		isNotifiedInitializing=true;
	}
	
	@Event(Environment.ON_INITIALIZED)
	public void onInitialized(Environment environment) {
		isNotifiedInitialized=true;
	}
}
