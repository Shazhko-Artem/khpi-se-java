package ua.khpi.shazhko_artem.task06.menu.commands;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import ua.khpi.shazhko_artem.task02.FileBasedRepository;
import ua.khpi.shazhko_artem.task02.Repository;
import ua.khpi.shazhko_artem.task02.biology.FissionSimulation;
import ua.khpi.shazhko_artem.task02.biology.FissionSimulation.FissionResult;
import ua.khpi.shazhko_artem.task06.views.ViewResult;

class MaxAliveEukaryotesPercentageCommandTest {

	@Test
	void getResult_MaxAliveEukaryotesPercentage_AffterInitView() throws ClassNotFoundException, IOException {
		String fileName = "task06.menu.commands.max.ini";
		Repository repository = new FileBasedRepository(fileName);
		ViewResult view = new ViewResult(repository, 1);
		view.init();
		view.save();

		@SuppressWarnings("unchecked")
		ArrayList<FissionSimulation.FissionResult> fissionResults = (ArrayList<FissionSimulation.FissionResult>) repository
				.load();
		assertNotNull(fissionResults);

		double expected = fissionResults.get(0).getAliveEukaryotesPercentage();

		for (FissionResult fissionResult : fissionResults) {
			double aliveEukaryotesPercentage=fissionResult.getAliveEukaryotesPercentage();
			if (expected < aliveEukaryotesPercentage)
				expected =aliveEukaryotesPercentage;
		}

		MaxAliveEukaryotesPercentageCommand command = new MaxAliveEukaryotesPercentageCommand(view);
		command.execute();
		double actual = command.getResult();

		assertEquals(expected, actual);
	}
}
