package ua.khpi.shazhko_artem.task05.menu.commands;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ua.khpi.shazhko_artem.task03.views.ViewResult;

class LoadCommandTest {

	@Test
	void getKey_Get5_InitWithParameter5() {
		int expectKey=5;
		ViewResult viewResult=new ViewResult();
		LoadCommand command=new LoadCommand(expectKey, viewResult);
		
		int actualKey= command.getKey();
		assertEquals(expectKey, actualKey);
	}
	
	@Test
	void toString_GetCommandName() {
		String expectString="Load.";
		ViewResult viewResult=new ViewResult();
		LoadCommand command=new LoadCommand(1, viewResult);
		
		String actualString= command.toString();
		assertEquals(expectString, actualString);
	}

}
