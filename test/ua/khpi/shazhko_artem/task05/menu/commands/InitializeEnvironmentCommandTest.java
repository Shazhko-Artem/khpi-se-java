package ua.khpi.shazhko_artem.task05.menu.commands;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ua.khpi.shazhko_artem.task03.views.ViewResult;

class InitializeEnvironmentCommandTest {

	@Test
	void getKey_Get5_InitWithParameter5() {
		int expectKey=5;
		ViewResult viewResult=new ViewResult();
		InitializeEnvironmentCommand command=new InitializeEnvironmentCommand(expectKey, viewResult);
		
		int actualKey= command.getKey();
		assertEquals(expectKey, actualKey);
	}
	
	@Test
	void toString_GetCommandName() {
		String expectString="Initialize environment.";
		ViewResult viewResult=new ViewResult();
		InitializeEnvironmentCommand command=new InitializeEnvironmentCommand(1, viewResult);
		
		String actualString= command.toString();
		assertEquals(expectString, actualString);
	}

}
